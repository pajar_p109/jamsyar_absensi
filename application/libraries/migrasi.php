<?php
class Migrasi{
	var $CI;
	var $DB;
	public function __construct(){
		$this->CI = &get_instance();
	}
	public function connect(){

		$this->DB	= $this->CI->load->database('destination',TRUE);
		$conected 	= $this->DB->initialize();
		if($conected)
		{
			return true;
		}
		else
		{ 
			return false;
		}							
	}
	
	// MIGRASI OPERASIONAL
		// PENCAPAIAN VOLUME IJP
		public function operasional_realisasi($data=array(),$periode)
		{
			$this->DB->trans_start();
			foreach($data as $row)
			{
				$this->DB->where(array('id_m_cabang'=>$row->id_m_cabang,'id_m_produk'=>$row->id_m_produk));
				$this->DB->update('t_pencapaian_vol_ijp',array('volume'=>$row->volume,'ijp_accrual'=>$row->ijp_accrual));
			}
			$this->DB->where(array('id_periode'=>1));
			$this->DB->update('t_periode',array('tgl_periode'=>$periode));
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
				
		}
		
		public function operasional_realisasi2($data=array(),$periode)
		{	
			$result = true;
			TRY 
			{
				$this->DB->trans_start();
				$result = $this->DB->query('truncate t_pencapaian_vol_ijp restart identity');
				if(!$result)
					throw new exception("GAGAL TRUNCATE");
				$result = $this->DB->insert_batch('t_pencapaian_vol_ijp',$data);
				if(!$result)
					throw new exception('GAGAL INSERT');
				
				$this->DB->where(array('id_periode'=>1));
				$result = $this->DB->update('t_periode',array('tgl_periode'=>$periode));	
				if(!$result)
					throw new exception('GAGAL UPDATE');
				$this->DB->trans_complete();
			}CATCH(exception $e){
				$result = FALSE;
			}
			if($result)
				return true;
			else 
				return false;
		}
		// RKAP
		public function operasional_rkap($data=array()){
			$this->DB->trans_start();
			foreach($data as $row)
			{
				$this->DB->where(array('id_m_cabang'=>$row->id_m_cabang,'id_m_produk'=>$row->id_m_produk,'flag_syariah'=>$row->flag_syariah));
				$this->DB->update('t_rkap_vol_ijp',array('volume'=>$row->volume,'ijp_accrual'=>$row->ijp));
			}
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
	// END OPERASIONAL
	
	// POSISI KEUANGAN
		PUBLIC FUNCTION keuangan_realisasi($data=array())
		{
			$this->DB->trans_start();
			// backup t_posisi_keuangan ke t_posisi_keuangan_bak
			$this->DB->query('insert into t_posisi_keuangan_bak(bulan,tahun,id_m_akun,nominal) select bulan,tahun,id_m_akun,nominal from t_posisi_keuangan');
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->update('t_posisi_keuangan',array('nominal'=>0));
				foreach($data as $row)
				{
					$where = array('id_m_akun'=>$row->id_m_akun);
					$data = array('bulan'=>$row->bulan,'tahun'=>$row->tahun,'nominal'=>$row->nominal);
					$this->DB->update('t_posisi_keuangan',$data,$where);
					
				}
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
		
		// RKAP
		PUBLIC FUNCTION keuangan_rkap($data=array())
		{
			$this->DB->trans_start();
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->update('m_posisi_keuangan',array('nominal'=>0));
				foreach($data as $row)
				{
					$where = array('id_m_akun'=>$row->id_m_akun);
					$data = array('tahun'=>$row->tahun,'nominal'=>$row->nominal);
					$this->DB->update('m_posisi_keuangan',$data,$where);
					
				}
				// Update periode
				/*
				$where 	= array('id_periode'=>3);
				$data 	= array('tgl_periode'=>$periode);
				$this->DB->update('t_periode',$data,$where);
				*/
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
			
	// END POSISI KEUANGAN
	// LABA RUGI
		// REALISASI
		PUBLIC FUNCTION laba_rugi_realisasi($data=array())
		{
			$this->DB->trans_start();
			$this->DB->query('insert into t_laba_rugi_bak(id_dc_wilayah_kerja,tahun,id_m_akun,jumlah,bulan) select id_dc_wilayah_kerja,tahun,id_m_akun,jumlah,bulan from t_laba_rugi');
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->update('t_laba_rugi',array('jumlah'=>0));
				foreach($data as $row)
				{
					$where = array('id_m_akun'=>$row->id_m_akun);
					$data = array('bulan'=>$row->bulan,'tahun'=>$row->tahun,'jumlah'=>$row->nominal);
					$this->DB->update('t_laba_rugi',$data,$where);
					
				}
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
		
		// RKAP
		PUBLIC FUNCTION laba_rugi_rkap($data=array())
		{
			$this->DB->trans_start();
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->update('t_laba_rugi_rkap',array('jumlah'=>0));
				foreach($data as $row)
				{
					$where = array('id_m_akun'=>$row->id_m_akun);
					$data = array('tahun'=>$row->tahun,'jumlah'=>$row->nominal);
					$this->DB->update('t_laba_rugi_rkap',$data,$where);
					
				}
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
	// END LABA RUGI
	// AKUNTANSI 
		// REALISASI	
		
		PUBLIC FUNCTION akuntansi_realisasi($investasi=array(),$klaim=array(),$subrogasi=array(),$periode)
		{
			$tbl_investasi 	= 't_investasi';
			$tbl_klaim		= 't_klaim';
			$tbl_subrogasi	= 't_subrogasi';
			
			$this->DB->trans_start();
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->truncate($tbl_investasi);
			$this->DB->truncate($tbl_klaim);
			$this->DB->truncate($tbl_subrogasi);
			
			$this->DB->insert_batch($tbl_investasi,$investasi);
			$this->DB->insert_batch($tbl_klaim,$klaim);
			$this->DB->insert_batch($tbl_subrogasi,$subrogasi);
			
			$this->DB->where(array('id_periode'=>2));
			$this->DB->update('t_periode',array('tgl_periode'=>$periode));
			
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
		
		// RKAP
		PUBLIC FUNCTION akuntansi_rkap($investasi=array(),$klaim=array(),$subrogasi=array())
		{
			$tbl_investasi 	= 't_rkap_investasi';
			$tbl_klaim		= 't_rkap_klaim';
			$tbl_subrogasi	= 't_rkap_subrogasi';
			
			$this->DB->trans_start();
			// sebelum update data, nominal di 0 dulu semuanya
			$this->DB->truncate($tbl_investasi);
			$this->DB->truncate($tbl_klaim);
			$this->DB->truncate($tbl_subrogasi);
			
			$this->DB->insert_batch($tbl_investasi,$investasi);
			$this->DB->insert_batch($tbl_klaim,$klaim);
			$this->DB->insert_batch($tbl_subrogasi,$subrogasi);
			
			$this->DB->trans_complete();
			if($this->DB->trans_status() !==FALSE)
				return true;
			else
				return false;
		}
	// END AKUNTANSI	
	
}
