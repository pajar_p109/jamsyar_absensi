<?php
class Uploads{
	var $ci;
	var $config;
	var $message;	
	public function __construct(){
		
		$this->ci = &get_instance();
		$this->config =  array(
                  'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/pub/uploads/",
                  'upload_url'      => base_url()."uploads/",
                  'allowed_types'   => "xls|xlsx",
                  'overwrite'       => TRUE,
                  'max_size'        => "1000KB"    
                );
		$this->message = array();		
	}
	public function do_upload($file){
		$this->ci->load->library('upload',$this->config);
		if($this->ci->upload->do_upload($file))
        {
			$this->message['msg'] 				= "File Uploaded Successfully";	
			$this->message['status']			= TRUE;
            $this->message["upload_file"] 		= $this->ci->upload->data();
        }
        else
        {
            $this->message['msg'] 				= "File Uploaded FAILED";	
			$this->message['status']			= FALSE;
            $this->message["upload_file"] 		= $this->ci->upload->display_errors();
        }
		return $this->message;	
	}
}