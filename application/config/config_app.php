<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Customized configuration...
|--------------------------------------------------------------------------
*/
// Pengganti dari dd_konfigurasi
$config['APP_CONFIG'] = array(
	'app_name' => 'MOBILE :: JAMKRINDO',
	'comp_name' => 'PERUM JAMINAN KREDIT INDONESIA',
	'short_comp_name' => 'PERUM JAMKRINDO',
	'address' => 'JL. Angkasa Blok B-9 Kav. 6 Kota Baru Bandar Kemayoran',
	'id_dd_kota' => '1',
	'zip_code' => '10720',
	'telephone' => '(021) 6540335',
	'fax' => '(021) 6540348, 6540344',
	'director' => 'Nahid Hudaya',
	'logo' => 'icpr_logo.png',
	'css_folder' => 'pub/css',
	'img_folder' => 'pub/images',
	'js_folder' => 'pub/js',
	'layout_folder' => 'system/application/views/_layouts'
);

$config['ACL_CONST'] = array(
	'acl_vie' => 1,
	'acl_add' => 2,
	'acl_edt' => 4,
	'acl_del' => 8,
	'acl_pro' => 16
);
