<?php
class User_m extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function list_user($limit=0,$offset=0)
	{
		$sql = 'SELECT 
					a.id_m_user,
					a.user_name,
					a.npp,
					a.flag_active,
					b.id_m_kelompok,
					b.kelompok,
					c.id_m_cabang,
					c.nama_cabang
				FROM 
					m_user a
				JOIN m_kelompok b ON a.id_m_kelompok = b.id_m_kelompok
				LEFT JOIN m_cabang c ON a.id_m_cabang = c.id_m_cabang	
				';
		if($limit != 0)
			$sql .= " LIMIT {$limit} OFFSET {$offset} ";
		return $this->db->query($sql);		
	}
	
	public function get_menu($limit=0,$offset=0){
		$sql = "SELECT 
					a.*,b.total 
				FROM 
					m_menu a 
				LEFT JOIN
					(SELECT
						COALESCE(COUNT(a.parent),0) as total,
						max(a.parent) as parent
					FROM
						m_menu a
					WHERE
						a.parent !=0
					GROUP BY a.parent	
							
					)b on a.id_m_menu = b.parent
				WHERE 
					a.parent = 0
				ORDER BY a.ordering asc	
				LIMIT_OFFSET				
				";
		if($limit != 0)
			$sql = str_replace('LIMIT_OFFSET'," LIMIT {$limit} OFFSET {$offset} ",$sql);
		else
			$sql = str_replace('LIMIT_OFFSET'," ",$sql);
		return $this->db->query($sql);					
	}
	
	public function list_menu($limit=0,$offset=0){
		$sql ="	SELECT
					a.id_m_menu,
					a.nama_menu as sub_menu,
					a.link,
					b.nama_menu as menu,
					COALESCE(c.total,0) as total	
				FROM 
					m_menu a 
				LEFT JOIN m_menu b ON a.parent = b.id_m_menu 
				LEFT JOIN (
					SELECT
						count(permission) as total,
						id_m_menu
					FROM m_acl
					WHERE permission !=0
					GROUP BY id_m_menu													
				)c on c.id_m_menu = a.id_m_menu	 
				WHERE 
					a.parent !=0	
				ORDER BY b.ordering,a.ordering
					LIMIT_OFFSET	
			";
		if($limit !=0)
			$sql = str_replace('LIMIT_OFFSET'," LIMIT {$limit} OFFSET {$offset}",$sql);
		else
			$sql = str_replace('LIMIT_OFFSET'," ",$sql);
		return $this->db->query($sql);	
	}
	
	public function get_menu_by_parent($parent,$id_kelompok){
		$sql = "	select a.*,coalesce(b.permission,0) as permission
					from m_menu a 
						LEFT JOIN(
							SELECT
								id_m_menu,
								max(permission) as permission
							FROM m_acl
							WHERE id_m_kelompok = {$id_kelompok}
							GROUP BY id_m_menu
						) b on b.id_m_menu = a.id_m_menu	
					where a.parent = {$parent} order by a.ordering";
		return $this->db->query($sql);
	}
	public function get_parent_menu($id_kelompok){
		$sql = "SELECT	a.*,
						coalesce(b.permission,0) as permission
				FROM m_menu a
				LEFT JOIN(
							SELECT
								id_m_menu,
								max(permission) as permission
							FROM m_acl
							WHERE id_m_kelompok = {$id_kelompok}
							GROUP BY id_m_menu
						) b on b.id_m_menu = a.id_m_menu	
				where a.parent = 0 order by a.ordering";
		return $this->db->query($sql);
	}
	
	public function submenu_by_id($id)
	{
		$sql = "	SELECT
						a.id_m_menu as id_sub_menu,
						a.nama_menu as sub_menu,
						a.link as url,
						a.parent as induk,
						a.ordering,
						b.id_m_menu,
						b.nama_menu as menu
					FROM
						m_menu a 
					LEFT JOIN 
						m_menu b ON a.parent = b.id_m_menu
					WHERE
						a.id_m_menu = {$id}
				";
		return $this->db->query($sql);		
	}
	
	public function acl_by_id($id_kelompok){
		$sql = "SELECT
					a.id_m_menu,
					a.nama_menu as sub_menu,
					b.nama_menu as menu,
					c.permission
				FROM m_menu a 
				JOIN m_menu b on a.parent = b.id_m_menu
				LEFT JOIN 
					(
						SELECT
							id_m_menu,
							max(permission) as permission
						FROM m_acl
						WHERE id_m_kelompok = {$id_kelompok}
						GROUP BY id_m_menu
					) c on c.id_m_menu = a.id_m_menu
				ORDER BY b.ordering,a.ordering
			";
		return $this->db->query($sql);	
	}

	
	public function get_kelompok($limit=0,$offset=0){
		$sql = "SELECT 
					a.*,
					(b.total+coalesce(c.tot,0)) as total
				FROM m_kelompok a 
				LEFT JOIN (SELECT 
								id_m_kelompok,
								sum(permission) as total 
							FROM m_acl group by id_m_kelompok
						  ) b on a.id_m_kelompok = b.id_m_kelompok 
				LEFT JOIN (SELECT
								id_m_kelompok,
								coalesce(count(id_m_kelompok),0) as tot 
							FROM m_user  where flag_active = true 
							GROUP BY id_m_kelompok
							)c on a.id_m_kelompok  = c.id_m_kelompok
							ORDER BY a.id_m_kelompok
				";
			if($limit != 0)
				$sql .= " LIMIT {$limit} OFFSET {$offset} ";
			
		return $this->db->query($sql);		
	}
}
