<?php
class DbDestination extends CI_Model{
	private $dbase;
	function __construct(){
		parent::__construct();
		$this->dbase = $this->load->database('db_destination',true);
	}
	public function insert($tbl,$data){
	}
	public function get($tbl){
		$this->dbase = $this->load->database('db_destination',true);
		return $this->dbase->get($tbl)->result();
	}
	public function update($query_update,$params){
		if($this->dbase->query($query_update,$params))
			return true;
		else
			return false;
	}
	
	public function update_ijp($data){
		$this->dbase->trans_start();
		foreach($data as $id_dc_wilayah_kerja=>$product)
		{
			foreach($product as $id_dc_product=>$ijp_accrual)
			{
				$sql = "UPDATE t_pencapaian_vol_ijp SET ijp_accrual = ? WHERE id_m_cabang = ? AND id_m_produk = ?";
				$arr = array(
					round($ijp_accrual, 8),
					$id_dc_wilayah_kerja,
					$id_dc_product
				);		
				$this->dbase->query($sql, $arr);
			}
		}
		$this->dbase->trans_complete();
	}
	public function update_ijp_kur($data){
		$this->dbase->trans_start();		
		foreach ($data as $id_dc_wilayah_kerja=>$ijp_accrual)
		{
			$sql = "UPDATE t_pencapaian_vol_ijp SET ijp_accrual = ? WHERE id_m_cabang = ? AND id_m_produk = 15";
			$arr = array(
				round($ijp_accrual, 8),
				$id_dc_wilayah_kerja
			);		
			$this->dbase->query($sql, $arr);
		}
		
		$this->dbase->trans_complete();
	}
	
	public function update_volume($data){
		foreach($data as $id_dc_wilayah_kerja=>$product)
		{
			foreach($product as $id_dc_product=>$volume)
			{
				$sql = "UPDATE t_pencapaian_vol_ijp SET volume = ? WHERE id_m_cabang = ? AND id_m_produk = ?";
				$arr = array(
					round($volume, 8),
					$id_dc_wilayah_kerja,
					$id_dc_product
				);	
				$this->dbase->query($sql, $arr);
				
			}				
		}
	}
	
	public function update_peride(){
		$sql = "UPDATE t_periode SET tgl_periode='".date('Y-m-d H:i:s')."' WHERE id_periode=1";
		$this->dbase->query($sql);
	}
	
}