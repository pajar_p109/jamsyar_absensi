<?php 
class menu_m extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	public function get_menu($id_m_kelompok){
		$sql = "SELECT 
					id_m_menu,
					nama_menu,
					link,
					icon	
				FROM 
					m_menu 
				WHERE 
					id_m_menu in( 	SELECT 
										a.parent 
									FROM 
										m_menu a 
									JOIN m_acl b on a.id_m_menu = b.id_m_menu 
									WHERE b.id_m_kelompok = ? AND b.permission !=0 group by a.parent
								)
					OR 
					id_m_menu in (SELECT 
										a.id_m_menu 
									FROM 
										m_menu a 
									JOIN m_acl b on a.id_m_menu = b.id_m_menu 
									WHERE b.id_m_kelompok = ? AND a.link != '#' AND a.parent = 0 AND b.permission !=0
									)				
				ORDER BY ordering
			";
		return $this->db->query($sql,array($id_m_kelompok,$id_m_kelompok));		
	}
	public function sub_menu($id_m_kelompok)
	{
		$sql = "SELECT 
					min(a.parent) as parent,
					max(a.nama_menu) as nama_menu,
					max(link) as link,
					max(icon) as icon	
				FROM m_menu a 
				JOIN m_acl b on a.id_m_menu = b.id_m_menu 
				WHERE 
					b.id_m_kelompok = ? 
					AND b.permission !=0 
				GROUP BY b.id_m_kelompok,b.id_m_menu 
				ORDER BY min(a.parent),min(a.ordering) 
			";
		return $this->db->query($sql,array($id_m_kelompok));	
	}
	
	public function menu_by_url($url)
	{
		$sql = " SELECT
					coalesce(b.nama_menu,a.nama_menu) as menu,
					a.nama_menu as sub_menu
				FROM 
					m_menu a
				LEFT JOIN m_menu b On a.parent = b.id_m_menu	
				WHERE lower(a.link) = '{$url}' 
			";
		return $this->db->query($sql);	
	}
	
	public function menu($id_m_kelompok){
		$sql = "SELECT 
					id_m_menu,
					nama_menu,
					link,
					icon	
				FROM 
					m_menu 
				WHERE 
					id_m_menu in( 	SELECT 
										a.parent 
									FROM 
										m_menu a 
									JOIN m_acl b on a.id_m_menu = b.id_m_menu 
									WHERE b.id_m_kelompok = ? AND b.permission !=0 group by a.parent
								)
					OR 
					id_m_menu in (SELECT 
										a.id_m_menu 
									FROM 
										m_menu a 
									JOIN m_acl b on a.id_m_menu = b.id_m_menu 
									WHERE b.id_m_kelompok = ? AND b.permission !=0 group by a.parent
									)	
				ORDER BY ordering
			";
		return $this->db->query($sql,array($id_m_kelompok));		
	}
	public function menu_sub($id_m_kelompok)
	{
		$sql = "SELECT 
					min(a.parent) as parent,
					max(a.nama_menu) as nama_menu,
					max(link) as link,
					max(icon) as icon	
				FROM m_menu a 
				JOIN m_acl b on a.id_m_menu = b.id_m_menu 
				WHERE 
					b.id_m_kelompok = ? 
					AND b.permission !=0 
				GROUP BY b.id_m_kelompok,b.id_m_menu 
				ORDER BY min(a.parent),min(a.ordering) 
			";
		return $this->db->query($sql,array($id_m_kelompok));	
	}
	
}
