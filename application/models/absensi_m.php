<?php 
class Absensi_m extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function list_absensi($limit=0,$offset=0,$date_start,$date_end,$name='')
	{
		$filter = array();	
		$sql = "SELECT 
					a.pegawai_id,
					count((case when jam_datang > '07:30:00;' then 1 END)) as telat,
					count((case when (jam_datang is null AND jam_pulang is not null) then 1 END)) as tdk_absen_datang,
					count((case when (jam_datang is not null AND jam_pulang is null) then 1 END)) as tdk_absen_pulang,
					count((case when (jam_datang is null AND jam_pulang is null) then 1 END)) as tdk_hadir,
					count((case when (lower(to_char(tanggal,'Dy')) !='sun' AND lower(to_char(tanggal,'Dy')) !='sat' )  then 1 END)) as hari_kerja,
					max(b.nama_pegawai) as nama,					
					count(absen_id) as total_absen	
				FROM	
					t_absen a,
					m_pegawai b					
				WHERE
					a.pegawai_id = b.pegawai_id
					AND tanggal BETWEEN '{$date_start}' AND '{$date_end}'
					NAME					
				GROUP BY 
					a.pegawai_id
				order by max(b.nama_pegawai)
				LIMIT_OFFSET	
				";
		if($name !='')
		{
			$sql = str_replace('NAME'," AND b.nama_pegawai like '%?%'",$sql);
			array_push($filter,$name);
			
		}else
			$sql = str_replace('NAME'," ",$sql);
		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
		return $this->db->query($sql,$filter);
	}
	function absen_for_month($limit=0,$offset=0,$id,$start,$end)
	{
		$sql = "SELECT 
					a.*,
					b.pegawai_id,
					b.nama_pegawai
				FROM	
					t_absen a
				JOIN	
					m_pegawai b ON a.pegawai_id = b.pegawai_id
				WHERE 
					b.pegawai_id = {$id}
					AND a.tanggal BETWEEN '{$start}' AND '{$end}'
					order by a.tanggal
					LIMIT_OFFSET
				";
		$filter = array();		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);		
		return $this->db->query($sql,$filter);		
	}
	
	function absen_by_id($id,$tanggal){
		$sql = "SELECT 
					pegawai_id,
					tanggal,
					jam_datang,
					jam_pulang,
					ket_telat,
					ket_tidak_absen_datang,
					ket_tidak_absen_pulang,
					ket_tidak_hadir,
					ket_lembur,
					surat_dokter,
					(case when (jam_datang is not null AND jam_datang > '07:30:00')then 1 else 0 END) as flag_telat,
					(case when (jam_datang is null AND jam_pulang is not null)then 1 else 0 END) as flag_tidak_absen_datang,
					(case when (jam_datang is not null AND jam_pulang is null)then 1 else 0 END) as flag_tidak_absen_pulang,
					(case when jam_datang is null AND jam_pulang is null then 1 END) as flag_tidak_hadir,
					(case when (jam_pulang > '16:30:00')then 1 else 0 END) as flag_lembur
					
				FROM 
					t_absen	
				where 	
					pegawai_id 	= {$id}
					AND tanggal = '{$tanggal}'	
				";
		return $this->db->query($sql);		
	}
}