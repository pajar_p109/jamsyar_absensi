<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('paging'))
{
    function paging($url,$uri_segment,$total,$perpage)
    {
		$CI =& get_instance();
        $this->CI->load->library('pagination');

		$config['base_url'] = 'http://example.com/index.php/test/page/';
		$config['total_rows'] = 200;
		$config['per_page'] = 20;

		$this->CI->pagination->initialize($config);

		return $this->CI->pagination->create_links();
    }   
}