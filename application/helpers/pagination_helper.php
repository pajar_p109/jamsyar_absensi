<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('paging'))
{
    function paging($url,$page,$total,$per_page=10)
    {
		$CI =& get_instance();
        $CI->load->library('pagination');
		
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['base_url'] = base_url().$url;
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page;
		
		$config['num_links'] = 9;
		$config['use_page_numbers'] = TRUE;
		
		
		$config['cur_page'] = ($page!='')? $page : 0;
		$CI->pagination->initialize($config);

		return $CI->pagination->create_links();
    }   
}