<?php

class Acl extends MY_Controller{

	private $_arrDescACL = array(
		"acl_vie" => "VIEW",
		"acl_add" => "ADD",
		"acl_edt" => "EDIT",
		"acl_del" => "DELETE",
		"acl_pro" => "PROSES"
	);
	public function __construct(){
		parent::__construct();
		$this->load->model(array('common_m','user_m'));
	}
	
	public function index(){
		if (!checkACL(ACL_VIE)) show_error(ERROR_200);
		$id_kelompok = $this->getVar('id_kelompok');
		$id = isset($id_kelompok) ? $id_kelompok : 0;
		
		$parent = $this->user_m->get_parent_menu($id)->result();
		
		$menus = array();
		$j = 0;
		foreach($parent as $row)
		{
			$menus[$j]['menu']	= $row->nama_menu;
			$menus[$j]['url'] 	= $row->link;
			$menus[$j]['icon']	= $row->icon;
			$menus[$j]['title']	= $row->title;
			$menus[$j]['id']	= $row->id_m_menu;
			$menus[$j]['tree']	= 'parent';
			$menus[$j]['permission'] = $row->permission;
			$j++;
			
			$subs = $this->user_m->get_menu_by_parent($row->id_m_menu,$id)->result();
			//$sub_arr = array();
			$i=0;
		
			foreach($subs as $sub)
			{
				$menus[$j]['menu']	= $sub->nama_menu;
				$menus[$j]['url'] 	= $sub->link;
				$menus[$j]['icon']	= $sub->icon;
				$menus[$j]['title']	= $sub->title;
				$menus[$j]['id']	= $sub->id_m_menu;
				$menus[$j]['permission'] = $sub->permission;
				$menus[$j]['tree']	= 'child';
				$j++;
			}	
		}
		
		//$rows 	= $this->user_m->list_menu(0,0)->result();
		
		$kelompok	= $this->common_m->get_where('m_kelompok','*',' 1=1 order by id_m_kelompok asc')->result();
		$arrConstACL = $this->config->item('ACL_CONST');
		arsort($arrConstACL, SORT_NUMERIC);	
			
		$data = array();
		$data['edit']			= base_url().'admin/acl/editAct';
		$data['print']			= 'admin/acl/print';
		//$data['rows']			= $rows;
		$data['kelompok']		= $kelompok;
		$data['arrConstACL'] 	= $arrConstACL;
		$data['arrDescACL'] 	= $this->_arrDescACL;
		$data['menus']			= $menus;
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			//$id_kelompok = $this->getVar('id_kelompok');
			//$rows = $this->user_m->acl_by_id($id_kelompok);
			$data['menus']			= $menus;
			$this->load->view('admin/acl/table_view',$data);
		}else
			$this->template->load('admin/acl/index',$data);	
	}
	
	public function editAct(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$ids_menu 		= $this->getVar('id_m_menu');
			$chk 			= $this->getVar('chk');
			$id_kelompok 	= $this->getVar('ids');
			foreach ($ids_menu as $i=>$id_menu) {
				
				$permission = 0;
				
				if(isset($chk[$i])) 
				{
					foreach ($chk[$i] as $v) $permission |= $v;	
				}
				//if($this->common_m->number_record('m_acl',"id_m_menu = {$id_menu} AND id_m_kelompok = {$id_kelompok}") > 0) // update
					$result = $this->common_m->update_where('m_acl',array('permission'=>$permission),array('id_m_menu'=>$id_menu,'id_m_kelompok'=>$id_kelompok));
				//else // INSERT
					//$result = $this->common_m->inserted('m_acl',array('id_m_menu'=>$id_menu,'id_m_kelompok'=>$id_kelompok,'permission'=>$permission));
				if(!$result)
					throw new exception('Proses update gagal!');
			}

			$message = "Proses Update Berhasil!";	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));	
	}
	
	public function cetak(){
		$id = $this->uri->segment(4);
		// 	DATA
			$rows = $this->user_m->acl_by_id($id)->result();
			$arrConstACL = $this->config->item('ACL_CONST');
			arsort($arrConstACL, SORT_NUMERIC);	

		// 	REPORT		
		$mainCols 		  = array();

		$arrCol 		  = array();
		$arrCol['title']  = 'NO.';
		$arrCol['width']  = 10;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '1';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'MENU';
		$arrCol['width']  = 60;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '2';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'SUBMENU';
		$arrCol['width']  = 60;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'PROSES';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label']  = '4';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'DELETE';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label'] = '5';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'EDIT';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label'] = '6';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'ADD';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label']  = '7';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'VIEW';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label']  = '8';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$params 			   = array();
		$params['arrHead'] 	   = $mainCols;
		$params['orientation'] = 'P';
		$params['format'] 	   = 'A4';
		$this->load->library('Report', $params);
		
		$this->report->Open();
		$this->report->AddPage();
		
		$no = 1;
		$mn='';
		foreach($rows as $row)
		{
			if($mn != $row->menu)
			{
				$mn 		= $row->menu;
				$disp_menu 	= $row->menu;
			}else
				$disp_menu = '';
				
			$arrData = array();
			$arrData[] = $no++;
			$arrData[] = ucwords($disp_menu );
			$arrData[] = ucwords($row->sub_menu);
			foreach($arrConstACL as $k=>$v) 
			{
				if (($row->permission & $v) == $v)
					$arrData[] = "X";
			}
			$this->report->InsertRow($arrData);
		}
			
		$this->report->ShowPDF('access_control_' . time());
	}
}