<?php
class Auth extends CI_Controller{
	private $flagLogin;
	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->flagLogin = $this->session->userdata('flag_login');
	}
	public function index(){
		if (!$this->flagLogin) {
			$this->load->view('login');
		}else
		{
			redirect('/dashboard/');
		}		
	}
	public function login(){
		$result 	= true;
		$message 	= '';
		$url 		= '';
		try
		{
			
			$user_name	= $this->input->post('user_name');
			$pass_word	= md5($this->input->post('pass_word'));
			$user 		= $this->common_m->get_where('m_user a join m_kelompok b on a.id_m_kelompok = b.id_m_kelompok','a.id_m_user,a.user_name,a.npp,b.id_m_kelompok,b.kelompok'," a.user_name = '{$user_name}' AND a.pass_word = '{$pass_word}' and a.flag_active = TRUE");
			$url        = '';
			if($user->num_rows()==0)
				throw new exception('Periksa kembali kombinasi username dan password Anda!');
			$usr = $user->row();
				// cek user log
				//$user_log = $this->common_m->get_where('m_user_log','*'," id_m_user = {$usr->id_m_user} order by login_time desc");
				/*if($user_log->num_rows() > 0 )
				{
					if(is_null($user_log->row()->logout_time))
					{
						$intv = strtotime(date('Y-m-d H:i:s')) - $user_log->row()->login_time;
						$waktu = ((($intv % 604800)%86400)/3600);
						if($waktu < 1)
							throw new exception('Masih ada user yang login!');
						else
						{
							$data = array('logout_time' => date('Y-m-d H:i:s'));
							$where = array('id_m_user_log'=>$user_log->row()->id_m_user_log);
							$result = $this->common_m->updated('m_user_log',$data,$where);
						}
					}
				}
				*/
				// insert user log
				
				$random_id = $this->randomString(10);
				$insert = array('id_m_user'	=> $usr->id_m_user,
								'login_time'=> date('Y-m-d H:i:s'),
								'random_id'	=> $random_id
								);
				$result = $this->common_m->inserted('m_user_log',$insert);
				if(!$result)
					throw new exception('log error..');
				
				// get hak askes berdasarkan menu dan submenu 
				
				
				$sess_arr 	= array();
				$sess_arr['flag_login'] 	= TRUE;
				$sess_arr['id_m_user'] 		= $usr->id_m_user;
				$sess_arr['user_name']	 	= $usr->user_name;
				$sess_arr['id_m_kelompok'] 	= $usr->id_m_kelompok;
				$sess_arr['kelompok'] 		= $usr->kelompok;
				$sess_arr['npp'] 			= $usr->npp;
				$sess_arr['id_login']		= $random_id;
				//$sess_arr['user_akses']		= $in_arr;
				
				$this->session->set_userdata($sess_arr);
				$url = base_url()."absen";
			
		}catch(exception $e)
		{
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message,'link'=>$url));				
	}
	public function logout()
	{
		try{
			$id_user 	= $this->session->userdata('id_m_user');
			$random_id 	= $this->session->userdata('id_login');
			$data = array('logout_time' => date('Y-m-d H:i:s'));
			$where = array('id_m_user'=>$id_user,'random_id'=>$random_id);
			$result = $this->common_m->updated('m_user_log',$data,$where);
			if(!$result)
				throw new exception('Gagal update user log');
			$this->session->sess_destroy();
			redirect();	
		}catch(exception $e)
		{
			$message = $e->getMessage();
			show_error($message,200);
		}
	}	
	function randomString($length = 6) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		//print_r($characters);
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		//print_r($str);
		return $str;
	}
	public function expiredPage(){
		$this->load->view('session_exp');
	}
}