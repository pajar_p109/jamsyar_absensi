<?php 
class Data extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model(array('common_m','absensi_m'));
		$this->load->helper(array('date','pagination'));
		
	}
	public function index($page=''){
		$bulan 			= $this->getVar('bulan') ? $this->getVar('bulan') : date('n');
		$tahun 			= $this->getVar('tahun') ? $this->getVar('tahun') : date('Y');	
		$list_for_month = listDayForMonth($bulan,$tahun);
		$date_start 	= $list_for_month[0];
		$date_end 		= end($list_for_month);	
		
		$limit 	= 10;
		$offset = $page =='' ? 0 : ($page-1)*$limit;
		$total 	= $this->absensi_m->list_absensi(0,0,$date_start,$date_end)->num_rows();	
        $rows 	= $this->absensi_m->list_absensi($limit,$offset,$date_start,$date_end);
		
		$data = array();
        $data['bulans'] = getListBulan();
        $data['bulan'] 	= $bulan;
        $data['tahun'] 	= $tahun;
		$data['rows']	= $rows->result();
		$data['total']	= $total;
		$data['page']	= paging('data/index',$page,$total,$limit);
		$data['no']		= $page == 0 ? 1 : ($page-1)*$limit+1;	
		
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$this->load->view('data/partial_index',$data);
		}else
			$this->template->load('data/index',$data);
	}
	function update($id,$bulan,$tahun,$page=''){
		
		$limit = 10;
		$offset = $page =='' ? 0 : ($page-1)*$limit;
		
		$dayForMonth = listDayForMonth($bulan,$tahun);
		$start_date = $dayForMonth[0];
		$end_date = end($dayForMonth);
		$total = $this->absensi_m->absen_for_month(0,0,$id,$start_date,$end_date)->num_rows();
		$rows = $this->absensi_m->absen_for_month($limit,$offset,$id,$start_date,$end_date);
		
		$row = $this->common_m->get_where('m_pegawai','*', " pegawai_id = {$id}")->row();
		$data = array();
		$data['rows']		= $rows->result();
		$data['total']		= $total;	
		$data['no']			= $offset;
		$data['pegawai']	= $row;
		$data['page']		= paging('data/update/'.$id.'/'.$bulan.'/'.$tahun,$page,$total,$limit);
		$this->template->load('data/edit',$data);
	}
	
	function edit_act(){
		try
		{	
			$result = true;

			$jam_datang 	= $this->getVar('jam_datang');
			$jam_pulang		= $this->getVar('jam_pulang');
			$pegawai_id 	= $this->getVar('pegawai_id');
			$tanggal 		= date('Y-m-d',strtotime($this->getVar('tanggal')));
			$jam_datang		= empty($jam_datang) ? NULL : date('H:i:s',strtotime($jam_datang));
			$jam_pulang		= empty($jam_pulang) ? NULL : date('H:i:s',strtotime($jam_pulang));
			$lembur			= $this->getVar('lembur');
			$telat			= $this->getVar('telat');
			$tidak_absen_datang = $this->getVar('tidak_absen_datang');
			$tidak_absen_pulang = $this->getVar('tidak_absen_pulang');
			$tidak_hadir		= $this->getVar('tidak_hadir');
			$surat_dokter		= $this->getVar('surat_dokter');
			
			$update = array('jam_datang' 				=> $jam_datang,
							'jam_pulang' 				=> $jam_pulang,
							'ket_telat' 				=> $telat,
							'ket_tidak_absen_datang'	=> $tidak_absen_datang,
							'ket_tidak_absen_pulang'	=> $tidak_absen_pulang,
							'ket_tidak_hadir'			=> $tidak_hadir,
							'ket_lembur'				=> $lembur,
							'surat_dokter'				=> $surat_dokter
								
								
							);
			$result = $this->common_m->updated('t_absen',$update,array('pegawai_id'=>$pegawai_id,'tanggal'=>$tanggal));
			if(!$result)
				throw new exception('Gagal update data absensi');
			
			$message = "Proses Update Berhasil!";	
			//print_r($update);
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));
	}
	
	public function get_absen_by_id()
	{
		$id_pegawai = $this->input->post('id_pegawai');
		$tanggal = $this->input->post('tanggal');
		$row = $this->absensi_m->absen_by_id($id_pegawai,$tanggal)->row();
		$data = array();
		$data['hari']		= hari_indo($row->tanggal);	
		$data['row'] 		= $row;
		$this->load->view('data/partial_edit',$data);
		
	}
	
	function get_alasan(){
		$id = $this->getVar('keterangan');
		$id_m_pegawai = $this->getVar('id_m_pegawai');	
		//$id = 4;
		if($id !=5)
			$alasan = array('tanpa keterangan','urusan kantor','urusan pribadi');
		else
			$alasan = array('tanpa keterangan','sakit','ijin','dinas luar','cuti');	
		$html = '';
		foreach($alasan as $key=>$als)
		{
			$html .= "<option value='".$key."'>".$als."</option>";
		}
		
		echo $html;
	}
}