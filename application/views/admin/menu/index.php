<div class="row top" style="min-height:40px;">
	<ul class='act'>
		<? if (checkACL(ACL_ADD)) { ?>
		<li id="add">
			<span class="add" title="Tambah User"></span>
		</li>
		<?php } if(checkACL(ACL_DEL) > 0) { ?>	
		<li id="delete" onClick="javascript:hapus()">
			<span class="delete" title="Hapus User"> </span>
		</li>
		<?php  } ?>
		<li id="print">
			<span class="print" title="Print"> </span>
		</li>
	</ul>
</div>

<!-- TITLE -->
<div class="row header_title">
  LIST MENU
</div>



<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax'>
	<div class='bg_table'>				
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class='tdNo'>NO</th>
					<td class='tdEdit'>EDIT</th>
					<th class='tdCheckbox'><input type='checkbox' name='select_all' id="select_all" onClick="javascript:checkItAll(this);"></th>
					<th width="30%">MENU</th>
					<th>URL</th>
					<th>TITLE</th>
					<th>ICON</th>
				</tr>
			</thead>
			<tbody id='tblBody'>
				<?php 
					foreach($rows as $row) 
					{
					
				?>
					<tr>
						<td><?php echo ++$no;?>.</td>
						<td>
							<a class='edit' href="javascript:openPop('/admin/menu/edit/<?php echo $row->id_m_menu?>','edit menu',400,330)">
								<img src="<?php echo base_url();?>pub/images/edit.ico" class='edit'>
							</a>
						</td>
						<td class='cek_box'>
							<?php if($row->total > 0){ ?>
								&nbsp;
							<?php }else{ ?>
								<input type='checkbox' name='id_menu[]' value='<?php echo $row->id_m_menu;?>' class="id_menu"></td>
							<?php } ?>	
						<td style='text-align:left;'><?php echo $row->nama_menu;?></td>
						<td style='text-align:left;'><?php echo $row->link;?></td>
						<td style='text-align:left;'><?php echo $row->title;?></td>
						<td style='text-align:left;'><?php echo $row->icon;?></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="8" class='tdCenter'>		
						<?=$page?>
					</td>
				</tr>
			</tfoot>	
			
		</table>
		
	</div>	
	</form>
</div>
<script>
function hapus()
{
	if(!check()){
		alert('Pilih data yang ingin dihapus!');
		return false;
	}
	var data = $("form#main_form").serialize();
	$.ajax({
		url 		: '<?php echo base_url()?>admin/menu/hapus',
		dataType	: 'json',
		data 		: data,
		type		: 'post',
		success		: function(response){
			alert(response.msg)
			location.reload();
		},error : function(){
			alert('Something error..');
		}		
	})
}
$("#update").click(function(){
		window.location.href = '<?=base_url()?>index.php/laba_rugi/realisasi/update';
	});
function check(){
		var status = false;
		var inputs = document.getElementsByClassName('id_menu')
		for (i = 0; i < inputs.length; i++) {
			if(inputs[i].checked)
			{
				status = true;
				break;
			}			
		}
		return status;		
	}	
</script>
