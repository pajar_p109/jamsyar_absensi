<body>
<div class="judul tdCenter">
      FORM EDIT MENU
</div>
<!-- content -->
<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax' action='<?=base_url()?>admin/submenu/editAct'>
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">	
			<tr>
				<td class='tdRight'>Menu</td>
				<td>
					<input type='text' name='menu' id='menu' class='form-control' value="<?php echo $row->menu;?>" disabled>	
				</td>
			</tr>
			<tr>
				<td class='tdRight'>Sub Menu</td>
				<td>
					<input type='text' name='sub_menu' id='sub_menu' class='form-control' value="<?php echo $row->sub_menu;?>" required>
					<input type="hidden" name="id_sub_menu" value="<?php echo $row->id_sub_menu; ?>"
				</td>
			</tr>
			<tr>
				<td class='tdRight'>URL</td>
				<td>
					<input type='text' name='url' id='url' class='form-control' value="<?php echo $row->url;?>" required>
				</td>
			</tr>
			<tr>
				<td class='tdRight'>Order</td>
				<td>
					<select name='ordering' class='form-control'>
					<?php
						foreach($orders as $order) 
						{ 
							if($row->ordering == $order->ordering)
								$selected = 'selected';
							else
								$selected = '';
					?>
						<option value='<?php echo $order->id_m_menu;?>' <?php echo $selected;?>><?php echo $order->ordering .' - '. $order->nama_menu;?></option>
					<?php } ?>
					</select>
				</td>
			</tr>
			 <tr>
				<td>&nbsp;</td>
				<td colspan='3'>
					<input type='submit' value='SIMPAN' class="btn btn-primary" name='submit'>
					<input type='reset' value='RESET' class="btn btn-danger" id='reset'>
				</td>
			 </tr>
		</table>
	</form>
</div>
</body>
</html>
<script>
$("form#main_form").submit(function(e){
	var link = $(this).attr('action');
	var data = $(this).serialize();
	$.ajax({
		url : link,
		type : 'post',
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status)
			{
				alert(response.msg);
				opener.location.reload();
				window.close();
			}else
				alert(response.msg);
		},error: function(){
			alert('SESSION ANDA HABIS');
			window.close();
		}
	})
	e.preventDefault();
})
</script>