
<div class="row search" >
	<div class="col-sm-4">
		<table>
			<tr>
				<td>Hari</td>
				<td><input type="text" readonly value="<?php echo $hari;?>"></td>	
			</tr>
			<tr>
				<td>Tanggal</td>	
				<td style="width:120px;">	
					<input type="text" name="tanggal" value="<?=date('d-m-Y',strtotime($row->tanggal))?>" readonly>
				</td>
			</tr>
			<tr>
				<td>Jam Datang</td>
				<td style="width:80px;"> <input type="text" name="jam_datang" value="<?php echo is_null($row->jam_datang) ? '' : date('H:i:s',strtotime($row->jam_datang));?>" readonly></td>
			</tr>
			<tr>
				<td>Jam Pulang</td>
				<td><input type="text" name="jam_pulang" value="<?php echo is_null($row->jam_pulang) ? '' : date('H:i:s',strtotime($row->jam_pulang));?>" readonly></td>
			</tr>
		</table>
	</div>
	<div class="col-sm-4">
		<table>
			<?php if($row->flag_lembur) {?>
			<tr>
				<td>LEMBUR</td>
				<td>
					<select name="lembur">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->lembur as $key=>$value) { 
							$selected = strtolower($row->ket_lembur  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<?php } if($row->flag_telat) {?>
			<tr>
				<td>TELAT</td>
				<td>
					<select name="telat">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->telat as $key=>$value) { 
							$selected = strtolower($row->ket_telat  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>	
				</td>
			</tr>
			<?php } if($row->flag_tidak_absen_datang) {?>
			<tr>
				<td>TIDAK ABSEN DATANG</td>
				<td>
					<select name="tidak_absen_datang">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->tidak_absen_datang as $key=>$value) { 
							$selected = strtolower($row->ket_tidak_absen_datang  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<?php } if($row->flag_tidak_absen_pulang){?>
			<tr>
				<td>TIDAK ABSEN PULANG</td>
				<td>
					<select name="tidak_absen_pulang">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->tidak_absen_pulang as $key=>$value) { 
							$selected = strtolower($row->tidak_absen_pulang  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<?php } if($row->flag_tidak_hadir) {?>
			<tr>
				<td>TIDAK HADIR</td>
				<td>
					<select name="tidak_hadir" onchange="get_surat(this.value)" id="tidak_hadir">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->tidak_hadir as $key=>$value) { 
							$selected = strtolower($row->ket_tidak_hadir  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr id="surat_dokter" style="display:none;">
				<td>SURAT DOKTER</td>
				<td>
					<select name="surat_dokter">
						<option value="">-- Pilih Keterangan --</option>
						<?php foreach($this->surat_dokter as $key=>$value) { 
							$selected = strtolower($row->surat_dokter  == strtolower($key)) ? 'selected' : ''; 	
						?>
						<option value="<?php echo $key; ?>" <?php echo  $selected;?>><?php echo $value;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>	
			<?php } ?>
		</table>
	</div>
</div>
<div class="row top" style="padding:2px 10px;">
	<input type="submit" value="simpan" class="btn">
</div>

<script>
	$(document).ready(function(){
		var tidak_hadir = $("#tidak_hadir").val();
		if(tidak_hadir == 'sakit')
			$("#surat_dokter").show('slow');	
		
	})
	function get_surat(name){
		if(name == 'sakit')
		{
			$("#surat_dokter").show('slow');	
				
		}else
		{
			$("#surat_dokter").hide('slow');
		}
		
	}
</script>


	