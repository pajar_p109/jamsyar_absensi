<!DOCTYPE html>
<html lang="en">
	<head>
		<link href="<?php echo base_url()?>pub/css/themes/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/themes/font-awesome.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/themes/style.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/themes/style-responsive.css" rel="stylesheet">
	
		<script src="<?php echo base_url()?>pub/js/jquery.js"></script>
		<script src="<?php echo base_url()?>pub/js/themes/jquery.dcjqaccordion.2.7.js"></script>
		<script src="<?php echo base_url()?>pub/js/themes/jquery.scrollTo.min.js"></script>	
		<script src="<?php echo base_url()?>pub/js/themes/jquery.nicescroll.js"></script>	
		<script src="<?php echo base_url()?>pub/js/themes/common-scripts.js"></script>
		
	</head>
<body>	
<section id="container" >
	<!--header start-->
      <header class="header blue-bg">
		<!--
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            //logo start
            <a href="index.html" class="logo"><b>Jamkrindo Mobile</b></a>
            
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="login.html">Logout</a></li>
            	</ul>
            </div>
		-->	
        </header>
      <!--header end-->
	  
	   <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="<?php echo base_url()?>pub/images/android.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">DASHBOARDddd<BR>JAMKRINDO MOBILE</h5>
				  
              	  	
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>UI Elements</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="general.html">General</a></li>
                          <li><a  href="buttons.html">Buttons</a></li>
                          <li><a  href="panels.html">Panels</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Components</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="calendar.html">Calendar</a></li>
                          <li><a  href="gallery.html">Gallery</a></li>
                          <li><a  href="todo_list.html">Todo List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Extra Pages</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html">Blank Page</a></li>
                          <li><a  href="login.html">Login</a></li>
                          <li><a  href="lock_screen.html">Lock Screen</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Forms</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="form_component.html">Form Components</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Data Tables</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="basic_table.html">Basic Table</a></li>
                          <li><a  href="responsive_table.html">Responsive Table</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>Charts</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="morris.html">Morris</a></li>
                          <li><a  href="chartjs.html">Chartjs</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
	
	<!-- MAIN CONTENT -->	
	<section id="main-content">
        <section class="wrapper">
		
            <div class="row mt">
				<div class="col-lg-12">
					<p style='height:300px'>tes</p>
					<p style='height:300px'>tes</p>
					<p style='height:300px'>tes</p>
					<p style='height:300px'>tes</p>
				</div>
			</div>
		</section>
	</section>
	<!-- END MAIN CONTENT -->
		
	<!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 - TI JAMKRINDO
              <a href="index.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->  
</section>
</body>
</html>