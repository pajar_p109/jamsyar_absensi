	</head>
<body>
<section id="container" >
	<!--header start-->
      <header class="header blue-bg">	
        </header>
      <!--header end-->
	   <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse">
              <!-- sidebar menu start-->
                <ul class="sidebar-menu" id="nav-accordion">
              	  <h5 class="centered" style="padding-bottom:10px;">DASHBOARD<BR>JAMKRINDO MOBILE</h5>
				  
					<?php
						$menus = side_menu();
						$mn = actived();
						
						foreach($menus as $key=>$menu)
						{
							$menu_aktif = '';
							if(trim(strtolower($mn['menu'])) == trim(strtolower($menu['nama_menu']))) $menu_aktif='active';
								
								
					?>
							<li class="sub-menu" >
							<?php if($menu['link'] != '#') { ?>
								 <a href="<?php echo base_url().$menu['link']; ?>" class="<?php echo  $menu_aktif;?>">
							<?php } else {?>
							  <a href="javascript:;" class="<?php echo  $menu_aktif;?>">
							 <?php } ?> 
								  <i class="<?php echo $menu['icon'];?>"></i>
								  <span><?php echo ucwords($menu['nama_menu']);?></span>
							  </a>
									<?php 
										if(count($menu['sub_menu']) > 0)
										{
											foreach($menu['sub_menu'] as $key2=>$sub)
											{
												$sub_class='';
												if(trim(strtolower($mn['sub_menu'])) == trim(strtolower($sub['sub_menu']))) $sub_class='aktif';
									?>
												<ul class="sub">
													<li>
														<a href="<?php echo base_url().$sub['url']?>" class="<?php echo $sub_class;?>">
														 <i class="<?php echo $sub['icon'];?>"></i>
														<span><?php echo ucwords(strtolower($sub['sub_menu']));?></span>
															
														</a>
													</li>
												</ul>
									<?php		
											}
										}
									?>	
							</li>
					<?php	
						}
					?>	
					<li class="sub-menu">
							  <a href="<?=base_url()?>index.php/auth/logout" >
								  <i class="glyphicon glyphicon-ban-circle"></i>
								  <span>Logout</span>
							  </a>
					</li>		
              </ul>
			  <!-- sidebar menu end-->
          </div>
      </aside>
            
      <!--sidebar end-->
	  <!-- MAIN CONTENT -->	
	<section id="main-content">
     <section class="wrapper">