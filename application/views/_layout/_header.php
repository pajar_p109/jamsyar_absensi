<!DOCTYPE html>
<html lang="en">
	<head>
		<title>UI JAMKRINDO MOBILE</title>
		<link href="<?php echo base_url()?>pub/css/themes/bootstrap.css" rel="stylesheet">
		
		<link href="<?php echo base_url()?>pub/css/font-awesome/css/font-awesome.css" rel="stylesheet">
		
		<link href="<?php echo base_url()?>pub/css/themes/style.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/themes/style-responsive.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/common.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/loading.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/sweetalert.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/jquery.toastmessage.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/abadi.css" rel="stylesheet">
		
		<link href="<?php echo base_url()?>pub/css/jquery.dataTables.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/login.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/popup.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/tabel.css" rel="stylesheet">
		<script src="<?php echo base_url()?>pub/js/jquery.js"></script>
		
		<script src="<?php echo base_url()?>pub/js/themes/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>pub/js/themes/jquery.dcjqaccordion.2.7.js"></script>
		<script src="<?php echo base_url()?>pub/js/themes/jquery.scrollTo.min.js"></script>	
		<script src="<?php echo base_url()?>pub/js/themes/jquery.nicescroll.js"></script>
		
		<script src="<?php echo base_url()?>pub/js/themes/common-scripts.js"></script>
		<script src="<?php echo base_url()?>pub/js/sweetalert.min.js"></script>
		<script src="<?php echo base_url()?>pub/js/jquery.toastmessage.js"></script>
		<script src="<?php echo base_url()?>pub/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url()?>pub/js/jquery.bpopup.min.js"></script>
		<script src="<?php echo base_url()?>pub/js/jquery.modal.js"></script>
		<script src="<?php echo base_url()?>pub/js/site.js"></script>
		<script src="<?php echo base_url()?>pub/js/common.js"></script>
		
		
		
		
	</head>
